import 'package:catfacts/model/cat_fact.dart';
import 'package:catfacts/repository/cat_fact_repository.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  CatFactRepository catFactRepository = CatFactRepository();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fetch Data Example',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Fetch Data Example'),
        ),
        body: Center(
          child: FutureBuilder<List<CatFact>>(
            future: catFactRepository.refreshCatFacts(),
            builder: (context, catfact) {
              if (catfact.hasData) {
                return ListView.builder(
                  itemCount: catfact.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                        title: Text(catfact.data[index].text),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SingleCatFact(
                                        catFact: catfact.data[index],
                                      )));
                        });
                  },
                );
              } else if (catfact.hasError) {
                return Text("${catfact.error}");
              }

              // By default, show a loading spinner.
              return CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}

class SingleCatFact extends StatelessWidget {
  final CatFact catFact;

  SingleCatFact({Key key, @required this.catFact}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cat Fact'),
      ),
      body: Center(
        child: Text(catFact.text),
      ),
    );
  }
}
