class CatFact {
  final int id;
  final String text;

  CatFact({this.id, this.text});

  factory CatFact.fromDatabase(Map json) {
    return CatFact(
      id: int.parse(json['id']),
      text: json['text'],
    );
  }

  factory CatFact.fromJson(Map json) {
    return CatFact(
      text: json['text'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'text': text,
    };
  }
}

