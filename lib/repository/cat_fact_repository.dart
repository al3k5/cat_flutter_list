import 'dart:collection';
import 'dart:convert';

import 'package:catfacts/model/cat_fact.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class CatFactRepository {
  final String tableName = 'cat_fact';
  Future<http.Response> _fetchCatFacts() {
    return http.get('https://cat-fact.herokuapp.com/facts');
  }

  Future<List<CatFact>> refreshCatFacts() async {
    try{
      final response = await _fetchCatFacts();

      if (response.statusCode == 200) {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        LinkedHashMap<String, dynamic> body = jsonDecode(response.body);
        List<dynamic> data = body["all"];
        List<CatFact> list =
        data.map((dynamic item) => CatFact.fromJson(item)).toList();
        await deleteCatFacts();
        for (CatFact catFact in list) {
          await insertCatFact(catFact);
        }
        return list;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to load CatFacts');
      }
    } catch (e) {
      return await catFacts();
    }
  }

  // Open the database and store the reference.
  Future<Database> database() async {
    return openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), 'cat_facts_database.db'),

      onCreate: (db, version) {
        // Run the CREATE TABLE statement on the database.
        return db.execute(
          "CREATE TABLE $tableName(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, text TEXT)",
        );
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );
  }

  Future<void> insertCatFact(CatFact catFact) async {
    // Get a reference to the database.
    final Database db = await database();

    // Insert the Dog into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      tableName,
      catFact.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<CatFact>> catFacts() async {
    // Get a reference to the database.
    final Database db = await database();

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query(tableName);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      return CatFact(
        id: maps[i]['id'],
        text: maps[i]['text'],
      );
    });
  }

  Future<void> deleteCatFacts() async {
    // Get a reference to the database.
    final db = await database();

    // Remove the Dog from the Database.
    await db.delete(tableName);
  }
}
